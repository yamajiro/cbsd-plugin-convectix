# cbsd-plugin-convectix

Additional ConvectIX scripts for CBSD

this module uses python, please install first, e.g:

  `pkg install -y lang/python38`

To install module:

  - cbsd module mode=install convectix
  - echo 'convectix.d' >> ~cbsd/etc/modules.conf
  - cbsd initenv
